(function() {
    var Managers = require('../data_access/managers').Managers;

    function forgotPassword(rqst, res, next, helpers) {
        var ObjectID = require('mongodb').ObjectID;
        var id = rqst.params.id;
        var objid = new ObjectID(id);
        var uuid = helpers.generateUUID();
        console.log(objid);
        var moment = require('moment')
        var ts = moment(Date.now()).add(72,'h').valueOf()
        console.log(moment(ts))
        Managers.updateManagers({ _id: objid }, { $set: { uuid: uuid, expire: ts } }, helpers, function(response) {
            console.log(response)
            var username = response.value.username
            var subject = 'Reset Password'
            var body = 'Click in the link to reset password'
            var html = '<h1><u>Mean-Sample-App</u></h1><a href="http://localhost:5000/reset-password/' + uuid + '">Click here to reset </a>'
            helpers.sendEmail(username, subject, body, html);

            res.json(response);
        })
    }

    function resetPassword(rqst, res, next, helpers) {
        var uuid = rqst.params.uuid;
        var moment = require('moment')  
        var ct = moment(Date.now()).valueOf()        
        console.log(uuid, ct);

        Managers.getManagersList({uuid: uuid },{}, helpers, function(response) {

            if(response.length > 0) {
                var et = response[0].expire
                if(et > ct) {
                    res.json(response);
                }
                else {
                    res.json([]);
                }
                
            }
        })
    }

    function updatePassword(rqst, res, next, helpers) {
        var id = rqst.body._id
        var password = rqst.password
        const bcrypt = require('bcrypt');
        var hash = bcrypt.hashSync('myPassword', 10);
        var newPassword = hash;

        var ObjectID = require('mongodb').ObjectID;
        var objid = new ObjectID(id);

        Managers.updateManagers({ _id: objid }, { $set: { uuid: "", expire:"", password: newPassword } }, helpers, function(response) {
            console.log(response)
            res.json(response)
        })
    }


    exports.managerPassword = {
        forgotPassword: forgotPassword,
        resetPassword: resetPassword,
        updatePassword: updatePassword
    }
})()